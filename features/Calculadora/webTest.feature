@WEB_TEST_PRUEBA
Feature: CALCULADORAv
  Como usuario
  Quiero realizar alguna accion
  Para comprobar de que este Test funciona

  Background:
    Given Cargar la pagina Calculadora

  Scenario Outline: Poc de prueba automatiza - version 1
    Given Hacer la operacion "<firstNumber>" "<operator>" "<secondNumber>"
    Then Validar si la respuesta es correcta: "<respuesta>"
    Examples:

      | firstNumber | operator | secondNumber | respuesta |
      | 1           | +        | 2            | 3         |
      | 8           | /        | 4            | 2         |
      | 3           | *        | 3            | 9         |
      | 100         | -        | 15           | 85        |
