package com.bdd.page;

import com.paulhammant.ngwebdriver.*;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("http://juliemr.github.io/protractor-demo/")
public class CalculatorPage extends BasePage {

    @FindBy(xpath = "//input[@class='input-small ng-pristine ng-untouched ng-valid']")
    private WebElementFacade secondText;

    public void doOperation( String secondNumber) {

        sendKeyElement(secondText, secondNumber);

    }

    public String getAnswer() {
        return "";
    }
}
