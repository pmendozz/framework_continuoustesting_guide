package com.bdd.page;

import com.bdd.generic.WebDriverDOM;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

public class BasePage extends WebDriverDOM {

    @FindBy(xpath = "//div[@class='loader']")
    private WebElementFacade loaderElement;

    protected boolean isLoaderPresent(){
        return isElementPresent(loaderElement);
    }

}

