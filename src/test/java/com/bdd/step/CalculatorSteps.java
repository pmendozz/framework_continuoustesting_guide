package com.bdd.step;

import com.bdd.lib.WebDriverManager;
import com.bdd.page.CalculatorPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;


public class CalculatorSteps extends ScenarioSteps {

    private CalculatorPage calculatorPage;

    @Step
    public void cargarPaginaCalculadora() {
        calculatorPage.setDriver(WebDriverManager.setCustomDriver("chrome"));
        calculatorPage.open();
        calculatorPage.getDriver().manage().window().maximize();
        calculatorPage.waitForAngularRequestsToFinish();
    }

    @Step
    public void hacerOperacion(String firstNumber, String operator, String secondNumber) {
        calculatorPage.doOperation(secondNumber);
    }

    @Step
    public void validarRespuestaIncorrecta(String respuestaCorrecta) {
        Assert.assertNotEquals(calculatorPage.getAnswer(),respuestaCorrecta);
    }

    @Step
    public void validarRespuestaCorrecta(String respuestaIncorrecta){
        Assert.assertEquals(calculatorPage.getAnswer(), respuestaIncorrecta);
    }

}
