package com.bdd.stepDefinition;


import com.bdd.step.CalculatorSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;


public class CalculatorStepDefinition {

    @Steps
    private CalculatorSteps calculatorSteps;

    @Given("^Cargar la pagina Calculadora$")
    public void cargar_calculadora() {
        calculatorSteps.cargarPaginaCalculadora();
    }


    @Given("^Hacer la operacion \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void hacer_la_operacion(String firstNumber, String operator, String secondNumber) {
        calculatorSteps.hacerOperacion(firstNumber, operator, secondNumber);
    }

    @Then("^Validar si la respuesta es correcta: \"([^\"]*)\"$")
    public void validar_si_respuesta_es_correcta(String respuestaEsperada) {
        calculatorSteps.validarRespuestaCorrecta(respuestaEsperada);

    }

    @Then("^Validar si la respuesta es incorrecta: \"([^\"]*)\"$")
    public void validar_si_la_respuesta_es_incorrecta(String respuestaEsperada) {
        calculatorSteps.validarRespuestaIncorrecta(respuestaEsperada);

    }

}
